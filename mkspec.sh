#!/bin/bash

curl http://www.inet.no/dante/files/dante-1.4.3.tar.gz | tar -xzO dante-1.4.3/SPECS/dante.spec > dante.spec.orig
patch dante.spec.orig dante-spec-changes.diff -o dante.spec
